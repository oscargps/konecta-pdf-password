<?php
require "PasswordProtectPDF.php";
require "logClass.php";

function encryptPdfFile($file, $password)
{
    define('MB', 1048576);
    if ($file && $file['size'] < 5 * MB ) {
        $encryptedFileName = encrypt($file, $password);
        $log = new Log('file_name', 'my_php_page');
        $log->log_msg($file['name'] . '|' . $password . '|' . $encryptedFileName);
        return $encryptedFileName;
    } else {
        return false;
    }
}


function encrypt($file, $password)
{
    $date = date("YmdHis") . '-';
    $encrypted_dir = "encrypted/";
    $outputFile = $encrypted_dir . $date .$file['name'];
    try {
        $pdf = new PasswordProtectPDF($file["tmp_name"], $password);
        $pdf->setTitle("Test PDF")->output('F', $outputFile);
        return $outputFile;
    } catch (\Exception $e) {
        if ($e->getCode() == 267) {
            die('The compression on this file is not supported.');
        }
        die('There was an error generating the PDF.');
    }
}



