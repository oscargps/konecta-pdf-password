<?php
header('Access-Control-Allow-Origin:*');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if ($method == "OPTIONS") {
    die();
}
date_default_timezone_set('America/Bogota');

include 'upload.php';

switch ($method) {
    case 'POST':
        $password = $_POST['password'];
        $file = $_FILES['file'];
        $outputFile = encryptPdfFile($file, $password);
        if ($outputFile) {
            echo $outputFile;
        } else {
            http_response_code(404);
            die(mysqli_error($con));
            exit;
        }
        break;
}
